package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.Jogador;
import com.example.demo.entity.Ranking;
import com.example.demo.respository.JogadorRepository;
import com.example.demo.respository.RankingRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RankingRepositoryTest {

	@Autowired
	private RankingRepository rankRepo;

	@Autowired
	private JogadorRepository jogRepo;

	@Test
	public void deve_ser_encontrado_os_dados_de_um_jogador_especifico() {
		Optional<Jogador> optionalJog = jogRepo.findByNome("josé");
		assertThat(optionalJog.isPresent()).isTrue();

		Ranking rank = mock(Ranking.class);

//		assertThat(optionalJog.get()).isInstanceOf(Jogador.class);
		rank = rankRepo.findRankingByJogador(optionalJog.get()).get();
		assertThat(rank).isInstanceOf(Ranking.class);

		assertThat(rank.getVitorias()).isEqualTo(80);
		assertThat(rank.getPartidas()).isEqualTo(60);
		assertThat(rank.getJogador().getId()).isEqualTo(optionalJog.get().getId());

	}
	
	
}
