package com.example.demo;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.demo.controller.JogadorController;
import com.example.demo.entity.Jogador;
import com.example.demo.respository.JogadorRepository;
import com.example.demo.respository.RankingRepository;
import com.example.demo.service.JogadorService;
import com.example.demo.service.RankingService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
public class JogadorControllerTest {
	
	private MockMvc mockMvc;

	@InjectMocks
	private JogadorController jogaController;

	@MockBean
	JogadorRepository jogRepo;
 	
	@MockBean
	JogadorService jogService;
	
	@MockBean
	RankingService rankService;
	
	@MockBean
	RankingRepository rankRepo;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(jogaController).build();
	}

	@Test
	public void deve_retornar_o_jogador_quando_passar_o_id() throws Exception {

		Mockito.when(jogRepo.findById(1)).thenReturn(Optional.of(new Jogador(1, "José")));

		mockMvc.perform(MockMvcRequestBuilders.get("/jogador/{id}", 1).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.nome", Matchers.is("José")));
	}
	@Test
	public void deve_salvar_o_jogador() throws Exception {

		Jogador entity = new Jogador(4, "Ana");
		Mockito.when(jogService.save(entity)).thenReturn(new Jogador());

		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.post("/jogador")
						.contentType(MediaType.APPLICATION_JSON)
						.content(new ObjectMapper().writeValueAsString(entity)))
				.andDo(print()).andExpect(status().isOk()).andReturn();
		result.getResponse().getContentAsString().contains("Ana");
		
	}
	
}
