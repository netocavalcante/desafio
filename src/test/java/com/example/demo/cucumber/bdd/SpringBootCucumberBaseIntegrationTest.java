package com.example.demo.cucumber.bdd;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.example.demo.entity.Jogador;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class SpringBootCucumberBaseIntegrationTest {

	public static final String BASE_URI = "http://localhost:8080/";

	private RestTemplate restTemplate;

	@LocalServerPort
	protected int port;

	public SpringBootCucumberBaseIntegrationTest() {
		restTemplate = new RestTemplate();
	}

	private String jogadorEndPoint() {
		return "http://localhost" + ":" + port + "/jogador";
	}

	Jogador put(final Jogador jogador) {
		return restTemplate.postForEntity(jogadorEndPoint(), jogador, Jogador.class).getBody();
	}

	Jogador getContents() {
		return restTemplate.getForEntity(jogadorEndPoint(), Jogador.class).getBody();
	}

}