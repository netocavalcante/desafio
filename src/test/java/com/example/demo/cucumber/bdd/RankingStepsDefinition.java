package com.example.demo.cucumber.bdd;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.example.demo.entity.Jogador;
import com.example.demo.entity.Ranking;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RankingStepsDefinition extends SpringBootCucumberBaseIntegrationTest {

	private RestTemplate restTemplate = new RestTemplate();

	private ResponseEntity<Ranking> responseEntityRanking;
	private ResponseEntity<Jogador> responseEntityJogador;
	private ResponseEntity<List<Ranking>> responseEntityRankingOrdered;

	HttpHeaders headers = new HttpHeaders();

	@Given("The player of id {int}")
	public void the_player_of_id(Integer int1) {
		responseEntityJogador = restTemplate
				.getForEntity(BASE_URI.concat("jogador").concat("/").concat(int1.toString()), Jogador.class);
		assertThat(responseEntityJogador.getBody().getId()).isEqualTo(int1);
	}

	@When("He request to update the number of victory and matchs of player {int} to {int} and {int}")
	public void he_request_to_update_the_number_of_victory_and_matchs_of_player_to_and(Integer int1, Integer int2,
			Integer int3) {

		assertThat(responseEntityJogador.getBody().getId()).isEqualTo(int1);

		Ranking rank = this.createRanking(responseEntityJogador.getBody(), int2, int3);
		assertThat(rank).isNotNull();

		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Ranking> requestUpdate = new HttpEntity<Ranking>(rank, headers);
		responseEntityRanking = restTemplate.exchange(BASE_URI.concat("ranking/").concat(int1.toString()),
				HttpMethod.PUT, requestUpdate, Ranking.class);

	}

	@Then("The value value of victory and matchs must be {int} and {int}")
	public void the_value_value_of_victory_and_matchs_must_be_and(Integer match, Integer victory) {
		Ranking rank = responseEntityRanking.getBody();

		assertThat(rank.getPartidas()).isEqualTo(match);
		assertThat(rank.getVitorias()).isEqualTo(victory);

	}

	@Then("The server must return an {string}")
	public void the_server_must_return_an(String message) {

		assertThat(responseEntityRanking.hasBody()).isFalse();
		if (!responseEntityRanking.hasBody()) {
			message = "Invalid Data";
		}

	}

	@Given("The user request the ordered rank")
	public void the_user_request_the_ordered_rank() {

		headers.setContentType(MediaType.APPLICATION_JSON);

		responseEntityRankingOrdered = restTemplate.exchange(BASE_URI.concat("ranking/"), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Ranking>>() {
				});

	}

	@When("The server response with code {int}")
	public void the_server_response_with_code(Integer int1) {
		assertThat(responseEntityRankingOrdered.getStatusCodeValue()).isEqualTo(int1);
	}

	@Then("Verify if the rank is ordered by victory")
	public void verify_if_the_rank_is_ordered_by_victory() {
		List<Ranking> guestList = new ArrayList<Ranking>();
		guestList.addAll(responseEntityRankingOrdered.getBody());
		List<Ranking> listTemp = this.returnOrdered(guestList);
		
		assertThat(listTemp).isEqualTo(responseEntityRankingOrdered.getBody());
	}

	

	public Ranking createRanking(Jogador jogador, Integer match, Integer victory) {
		Ranking rank = new Ranking();
		rank.setJogador(jogador);
		rank.setPartidas(match);
		rank.setVitorias(victory);
		return rank;
	}

	public List<Ranking> returnOrdered(List<Ranking> guest) {

		List<Ranking> listTemp = guest;

		Collections.sort(listTemp, new Comparator<Ranking>() {

			@Override
			public int compare(Ranking o1, Ranking o2) {
				if (o1.getVitorias() > o2.getVitorias()) {
					return -1;
				} else {
					if (o1.getVitorias() < o2.getVitorias()) {
						return 1;
					} else
						return 0;
				}

			}

		});
		
		return listTemp; 
		
		
	}

}
