package com.example.demo.cucumber.bdd;

import static org.assertj.core.api.Assertions.assertThat;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.example.demo.entity.Jogador;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;

public class JogadorStepsDefinition extends SpringBootCucumberBaseIntegrationTest {

	private RestTemplate restTemplate = new RestTemplate();

	private ResponseEntity<Jogador> responseEntity;
	
	@Given("A player with id {int}")
	public void a_player_with_id(Integer int1) {
		responseEntity = restTemplate
				.getForEntity(BASE_URI.concat("jogador").concat("/").concat(int1.toString()), Jogador.class);
		assertThat(responseEntity.getBody().getId()).isEqualTo(int1);
		
	}

	@Then("A new player is created and receive {int} as response code")
	public void a_new_player_is_created_and_receive_as_response_code(Integer int1) {
		Jogador jog = new Jogador(5, "Carlos");

		responseEntity = restTemplate.postForEntity(BASE_URI.concat("jogador"), jog, Jogador.class);
		if (responseEntity.getStatusCode() != HttpStatus.OK) {
			throw new CucumberException("User not created");
		}

		assertThat(responseEntity.getStatusCode().value()).isEqualTo(int1);
	}

	@When("This player create a new player named {string}")
	public void this_player_create_a_new_player_named(String string) {
		Jogador jog = new Jogador(5, "Carlos");

		responseEntity  = restTemplate.postForEntity(BASE_URI.concat("jogador"), jog, Jogador.class);
		if (responseEntity.getStatusCode() != HttpStatus.OK) {
			throw new CucumberException("Nome Nao pode ser vazio");
		}

		
	}

	@Then("An error message must be returned")
	public void an_error_message_must_be_returned() {
		
	   assertThat(responseEntity.getBody()).isNotEqualTo(Jogador.class); 
	   if ( !(responseEntity.getBody() instanceof Jogador ) ) {
		   throw new CucumberException("Jogador nao foi criado");
	   }
	}

	
}
