package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.Jogador;
import com.example.demo.respository.JogadorRepository;
import com.example.demo.service.JogadorService;
import com.example.demo.service.except.UnicidadeJogadorException;
import com.example.demo.service.impl.JogadorServiceImplementation;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JogadorUnityTest {

	private static final String nome = "josé";

	@MockBean
	JogadorRepository jogRepo;

	private Jogador jogador;
	private JogadorService jogService;
	
	@Before
	public void setup() {
		jogService = new JogadorServiceImplementation(jogRepo);
		jogador = new Jogador();
		jogador.setNome(nome);
		
		when(jogRepo.findByNome(jogador.getNome())).thenReturn(Optional.empty());
		
	}

	@Test
	public void deve_salvar_jogador() throws Exception {
		
		jogService.save(jogador);
		verify(jogRepo).saveAndFlush(jogador);
	}
	
	@Test(expected = UnicidadeJogadorException.class) 
	public void  nao_deve_salvar_pessoa_com_o_mesmo_nome() throws Exception{
		when(jogRepo.findByNome(jogador.getNome())).thenReturn(Optional.of(jogador));
		jogService.save(jogador);
	}
	
	@Test
	public void deve_encotrar_uma_pessoa_por_id() throws Exception {
		when(jogRepo.findById(1)).thenReturn(Optional.of(jogador));
		Optional<Jogador> optional = jogRepo.findById(1);
		
		assertThat(optional.get().getNome()).isEqualTo("josé");
	}
	
	@Test
	public void deve_atualizar_uma_pessoa()  throws Exception {
		jogador.setId(1);
		jogador.setNome("Camilla");
		jogService.update(jogador);
		verify(jogRepo).save(jogador);
	}

}
