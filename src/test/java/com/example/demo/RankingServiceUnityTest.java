package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.Jogador;
import com.example.demo.entity.Ranking;
import com.example.demo.respository.RankingRepository;
import com.example.demo.service.JogadorService;
import com.example.demo.service.RankingService;
import com.example.demo.service.except.ListaDeRankingVaziaException;
import com.example.demo.service.except.RankingComValoresNegativosException;
import com.example.demo.service.except.UnicidadeDeJogadorPorRankingException;
import com.example.demo.service.impl.RankingServiceImplementation;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RankingServiceUnityTest {

	@MockBean
	RankingRepository rankRepo;

	
	
	private Ranking rank;

	private Jogador jogador;
	private RankingService rankService;
	
	@MockBean
	private JogadorService jogService;

	@Before
	public void setUp() {
		rankService = new RankingServiceImplementation(rankRepo,jogService);

		rank = new Ranking();
		jogador = new Jogador();
		jogador.setNome("José");
		rank.setJogador(jogador);
		rank.setPartidas(30);
		rank.setVitorias(30);

		when(rankRepo.findRankingByJogador(jogador)).thenReturn(Optional.empty());

	}

	@Test
	public void deve_ser_salvo_um_ranking() throws Exception {
		rankService.save(rank);
		verify(rankRepo).save(rank);
	}

	@Test(expected = RankingComValoresNegativosException.class)
	public void nao_deve_ser_persistido_ranking_com_valores_negativos() throws Exception {
		this.rank.setPartidas(-1);
		this.rank.setPartidas(-2);
		rankService.save(rank);
		verify(rankRepo).save(rank);
	}

	@Test(expected = UnicidadeDeJogadorPorRankingException.class)
	public void nao_deve_ser_criado_mais_de_um_ranking_para_o_mesmo_jogador() throws Exception {
		when(rankRepo.findRankingByJogador(rank.getJogador())).thenReturn(Optional.of(rank));

		rankService.save(rank);
	}

	@Test(expected = ListaDeRankingVaziaException.class)
	public void deve_ser_exibida_uma_exceção_em_caso_de_ranking_vazio() throws ListaDeRankingVaziaException {
		rankService.findAllByIdOrderByVitoriasDesc();
		verify(rankRepo).findAllByIdOrderByVitoriasDesc();

	}

	@Test()
	public void deve_ser_exibido_o_ranking_ordenado_por_vitorias() throws ListaDeRankingVaziaException,
			UnicidadeDeJogadorPorRankingException, RankingComValoresNegativosException {
		
		List<Ranking> rankings = new ArrayList<Ranking>();
		rankings.add(rank);
		
		when(rankRepo.findAllByIdOrderByVitoriasDesc()).thenReturn(rankings);
		
		rankings = rankService.findAllByIdOrderByVitoriasDesc();
		verify(rankRepo).findAllByIdOrderByVitoriasDesc();
		Jogador jogTest = rankings.get(0).getJogador();
		assertThat(jogTest.getNome()).isEqualTo("José");

	}

}
