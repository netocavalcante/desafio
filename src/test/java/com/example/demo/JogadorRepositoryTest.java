package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.Jogador;
import com.example.demo.respository.JogadorRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class JogadorRepositoryTest {

	@Autowired
	private JogadorRepository jogRepo;

	@Test
	public void deve_encontrar_pessoa_por_nome() throws Exception {

		Optional<Jogador> optional = jogRepo.findByNome("José");

		assertThat(optional.isPresent()).isTrue();

	}

}
