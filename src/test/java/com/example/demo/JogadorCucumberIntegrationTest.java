package com.example.demo;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/feature/jogador.feature", plugin = {"pretty", "html:target/cucumber"})
public class JogadorCucumberIntegrationTest {
}
