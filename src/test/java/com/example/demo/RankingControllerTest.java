package com.example.demo;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.demo.controller.RankingController;
import com.example.demo.entity.Jogador;
import com.example.demo.entity.Ranking;
import com.example.demo.respository.JogadorRepository;
import com.example.demo.respository.RankingRepository;
import com.example.demo.service.JogadorService;
import com.example.demo.service.RankingService;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
public class RankingControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private RankingController rankController;

	@MockBean
	RankingRepository rankRepo;
 	
	@MockBean
	JogadorRepository jogRepo;
 	
	@MockBean
	JogadorService jogService;
	
	@MockBean
	RankingService rankService;
		
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(rankController).build();
	}
	
	
	@Test
	public void deve_retornar_o_Ranking_quando_passar_o_id() throws Exception {
		Jogador jo1 = new Jogador(1,"Maria");
		
		Mockito.when(rankRepo.findById(1)).thenReturn(Optional.of(new Ranking(1 , jo1 , 2,2)));
	
		System.out.println(rankRepo);	
		
		mockMvc.perform(MockMvcRequestBuilders.get("/ranking/{id}" , 1).contentType(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk()).andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.partidas", Matchers.is(2)));
			
	}
		
	@Test
	public void deve_pegar_o_ranking_dos_jogadores_ordenado_por_vitorias() throws Exception {
		List<Ranking> rankings = new ArrayList<Ranking>();
		Jogador jog1 = new Jogador(1,"José");
		Jogador jog2 = new Jogador(2,"Maria");
		
		Ranking rank = new Ranking();
		rank.setJogador(jog1);
		rank.setPartidas(20);
		rank.setVitorias(20);
		
		Ranking rank2 = new Ranking();
		rank2.setJogador(jog2);
		rank2.setPartidas(30);
		rank2.setVitorias(30);
		
		rankings.add(rank);
		rankings.add(rank2);
		
		Mockito.when(rankService.findAllByIdOrderByVitoriasDesc()).thenReturn(rankings);
		rankings = rankService.findAllByIdOrderByVitoriasDesc();
		
		mockMvc.perform(MockMvcRequestBuilders.get("/ranking/"))
		.andExpect(status().isOk()).andDo(print())
		.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(2) ));
		
	}
}
