package com.example.demo;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/feature/ranking.feature", plugin = {"pretty", "html:target/cucumber"})
public class RankingCucumberIntegration {

}
