#Author: netocavalcante@gmail.com
@jogador
Feature: Adding a new player

Scenario: Add a player
    Given A player with id 1
    When This player create a new player named 'Carlos'
    Then A new player is created and receive 200 as response code
  


  Scenario: Validate the empty name of a player
   Given A player with id 1
    When This player create a new player named ' '
    Then An error message must be returned
