#Author: netocavalcante@gmail.com

@tag
Feature: Update value of Matches

  @tag1
  Scenario: Update Matches value
    Given The player of id 1 
    When He request to update the number of victory and matchs of player 1 to 60 and 80
    Then The value value of victory and matchs must be 60 and 80
    

  @tag2
   Scenario: Update Matches with wrong value
    Given The player of id 1 
    When He request to update the number of victory and matchs of player 1 to -10 and -130
    Then The server must return an 'error message'
    
    
  @tag3
  Scenario: Show the correct rank order
    Given The user request the ordered rank 
    When The server response with code 200 
    Then Verify if the rank is ordered by victory
    