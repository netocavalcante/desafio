
CREATE TABLE `jogador` (`id` bigint(20) NOT NULL AUTO_INCREMENT, `nome` varchar(255) DEFAULT NULL,  PRIMARY KEY (`id`));
CREATE TABLE `ranking` (`id` bigint(20) NOT NULL AUTO_INCREMENT, `partidas` int(11),  `vitorias` int(11) , `jogador_id` bigint(20),  PRIMARY KEY (`id`),  foreign key (jogador_id) References jogador(id) ); 

INSERT INTO jogador (nome) VALUES ('José');
INSERT INTO jogador (nome) VALUES ('Carlos');
INSERT INTO jogador (nome) VALUES ('Camila');

INSERT INTO ranking (vitorias, partidas, jogador_id) values (30,30,(SELECT id FROM jogador WHERE nome = 'José')); 
INSERT INTO ranking (vitorias, partidas, jogador_id) values (20,25,(SELECT id FROM jogador WHERE nome = 'Carlos')); 
INSERT INTO ranking (vitorias, partidas, jogador_id) values (15,35,(SELECT id FROM jogador WHERE nome = 'Camila')); 
