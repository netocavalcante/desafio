package com.example.demo.respository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Jogador;

public interface JogadorRepository extends JpaRepository<Jogador, Integer>{

	    Optional<Jogador> findByNome(String nome);
		
}
