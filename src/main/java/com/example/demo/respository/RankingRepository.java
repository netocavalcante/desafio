package com.example.demo.respository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.Jogador;
import com.example.demo.entity.Ranking;

public interface RankingRepository extends JpaRepository<Ranking, Integer> {
	
	public Optional<Ranking> findRankingByJogador(Jogador jogador);

	@Query("SELECT r FROM Ranking r ORDER BY r.vitorias DESC")
	public List<Ranking> findAllByIdOrderByVitoriasDesc();

	
}
