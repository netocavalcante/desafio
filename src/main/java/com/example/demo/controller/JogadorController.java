package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Jogador;
import com.example.demo.respository.JogadorRepository;
import com.example.demo.service.JogadorService;
import com.example.demo.service.except.JogadorComNomeVazaioException;
import com.example.demo.service.except.ListaDeJogadorVaziaException;
import com.example.demo.service.except.UnicidadeJogadorException;
 
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/jogador")
public class JogadorController {
	
	private JogadorService JogadorService;
	
	private JogadorRepository jogRepo;

	JogadorController(JogadorService JogadorService, JogadorRepository jogRepo){
		this.JogadorService = JogadorService;
		this.jogRepo = jogRepo;
	}
	
	@GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Jogador> buscarJogadorPorId() {

		List<Jogador> jogadores = null;
		try {
			jogadores = JogadorService.findAll();
		} catch (ListaDeJogadorVaziaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jogadores;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Jogador buscarJogadorPorId(@PathVariable(name = "id") Integer id) {
		Jogador jogador = null;
		Optional<Jogador> jogadorOptional = jogRepo.findById(id);
		if (jogadorOptional.isPresent()) {
			jogador = jogadorOptional.get();
		}
		return jogador;

	}

	@PutMapping(path = "/{jogador_id}", produces = "application/json")
	public @ResponseBody Jogador atualizarJogador(@PathVariable(name = "jogador_id") Integer id,
			@RequestBody @Valid Jogador jogador) {

		if (id != null) {

			Optional<Jogador> jogadorOptional = jogRepo.findById(id);
			if (jogadorOptional.isPresent()) {

				Jogador jogadorTemp = jogadorOptional.get();
				jogadorTemp.setNome(jogador.getNome());
				
				try {
					JogadorService.update(jogadorTemp);
				} catch (JogadorComNomeVazaioException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnicidadeJogadorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		return jogador;
	}

	@PostMapping()
	public @ResponseBody Jogador cadastrarJogador(@RequestBody @Valid Jogador jogador) {
	
		try {
			this.JogadorService.save(jogador);
		} catch (UnicidadeJogadorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JogadorComNomeVazaioException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jogador;

	}
}
