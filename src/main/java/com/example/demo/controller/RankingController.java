package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Ranking;
import com.example.demo.respository.RankingRepository;
import com.example.demo.service.RankingService;
import com.example.demo.service.except.JogadorComNomeVazaioException;
import com.example.demo.service.except.ListaDeRankingVaziaException;
import com.example.demo.service.except.RankingComValoresNegativosException;
import com.example.demo.service.except.UnicidadeDeJogadorPorRankingException;
import com.example.demo.service.except.UnicidadeJogadorException;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/ranking")
public class RankingController {

	private RankingService rankService;

	private RankingRepository rankRepo;

	 RankingController(RankingService rankService, RankingRepository rankRepo) {

		this.rankService = rankService;
		this.rankRepo = rankRepo;
	}

	@GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Ranking> buscarRankingPorId() {

		List<Ranking> ranking = new ArrayList<Ranking>();
		try {
			ranking = rankService.findAllByIdOrderByVitoriasDesc();
		} catch (ListaDeRankingVaziaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ranking;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Ranking buscarRankingPorId(@PathVariable(name = "id") Integer id) {
		Ranking ranking = null;
		Optional<Ranking> rankingOptional = rankRepo.findById(id);
		if (rankingOptional.isPresent()) {
			ranking = rankingOptional.get();
		}
		return ranking;

	}

	@PutMapping(path = "/{ranking_id}", produces = "application/json")
	public @ResponseBody Ranking atualizarRanking(@PathVariable(name = "ranking_id") Integer id,
			@RequestBody @Valid Ranking ranking) {
		Ranking rankResp = null;
		if (id != null) {

			try {
				rankResp = rankService.update(id, ranking);
			} catch (RankingComValoresNegativosException | UnicidadeDeJogadorPorRankingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rankResp;

	}

	@PostMapping()
	public @ResponseBody Ranking cadastrarRanking(@RequestBody @Valid Ranking ranking) {
		Ranking rankAtualizado = null;
		try {
			rankAtualizado = this.rankService.save(ranking);
		} catch (RankingComValoresNegativosException | UnicidadeDeJogadorPorRankingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnicidadeJogadorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JogadorComNomeVazaioException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rankAtualizado;

	}

}
