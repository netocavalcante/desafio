package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Jogador;
import com.example.demo.service.except.JogadorComNomeVazaioException;
import com.example.demo.service.except.ListaDeJogadorVaziaException;
import com.example.demo.service.except.UnicidadeJogadorException;


public interface JogadorService {

		public Jogador save(Jogador jogador) throws UnicidadeJogadorException, JogadorComNomeVazaioException;
		public List<Jogador> findAll() throws ListaDeJogadorVaziaException;
		public Jogador update(Jogador jogador) throws JogadorComNomeVazaioException, UnicidadeJogadorException;
}
