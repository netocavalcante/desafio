package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.entity.Jogador;
import com.example.demo.entity.Ranking;
import com.example.demo.respository.JogadorRepository;
import com.example.demo.respository.RankingRepository;
import com.example.demo.service.JogadorService;
import com.example.demo.service.RankingService;
import com.example.demo.service.except.JogadorComNomeVazaioException;
import com.example.demo.service.except.ListaDeRankingVaziaException;
import com.example.demo.service.except.RankingComValoresNegativosException;
import com.example.demo.service.except.UnicidadeDeJogadorPorRankingException;
import com.example.demo.service.except.UnicidadeJogadorException;

@Service
public class RankingServiceImplementation implements RankingService {

	private RankingRepository rankRepo;
	
	private JogadorService jogService;
	
	public RankingServiceImplementation(RankingRepository rankRepo,JogadorService jogService) {
		this.rankRepo = rankRepo;
		this.jogService = jogService;
	}

	@Override
	public Ranking save(Ranking rank)
			throws UnicidadeDeJogadorPorRankingException, RankingComValoresNegativosException, UnicidadeJogadorException, JogadorComNomeVazaioException {
		Optional<Ranking> optional = rankRepo.findRankingByJogador(rank.getJogador());
		Jogador jogTemp = new Jogador();
		
		if (rank.getPartidas() < 0 || rank.getVitorias() < 0) {
			throw new RankingComValoresNegativosException();
		}

		if (optional.isPresent()) {
			throw new UnicidadeDeJogadorPorRankingException();
		}
		else {
			jogTemp.setId(1);
			jogTemp.setNome(rank.getJogador().getNome());
			jogTemp = this.jogService.save(rank.getJogador());
		
		}
				
		rank.setJogador(jogTemp);
		
		return rankRepo.save(rank);
	}

	@Override
	public Ranking update(Integer id, Ranking ranking)
			throws RankingComValoresNegativosException, UnicidadeDeJogadorPorRankingException {

		Ranking rankResp = null;
		Ranking rankTemp = new Ranking();

		if (ranking.getPartidas() < 0 || ranking.getVitorias() < 0) {
			throw new RankingComValoresNegativosException();
		}

		Optional<Ranking> rankingOptional = rankRepo.findRankingByJogador(ranking.getJogador());

		if (rankingOptional.isPresent()) {

			rankTemp = rankingOptional.get();

			if (rankTemp.getId() != id) {
				throw new UnicidadeDeJogadorPorRankingException();
			}

			rankTemp.setPartidas(ranking.getPartidas());
			rankTemp.setJogador(ranking.getJogador());
			rankTemp.setVitorias(ranking.getVitorias());

		}

		rankResp = rankRepo.save(rankTemp);

		return rankResp;
	}

	@Override
	public List<Ranking> findAllByIdOrderByVitoriasDesc() throws ListaDeRankingVaziaException {
		// verificacaoes
		List<Ranking> rankings = rankRepo.findAllByIdOrderByVitoriasDesc();

		if (rankings.isEmpty()) {
			throw new ListaDeRankingVaziaException();
		}

		return rankings;
	}

}
