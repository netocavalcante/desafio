package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.entity.Jogador;
import com.example.demo.respository.JogadorRepository;
import com.example.demo.service.JogadorService;
import com.example.demo.service.except.JogadorComNomeVazaioException;
import com.example.demo.service.except.ListaDeJogadorVaziaException;
import com.example.demo.service.except.UnicidadeJogadorException;

@Service
public class JogadorServiceImplementation implements JogadorService {
	
	private final JogadorRepository jogRepo;
	
	
	public JogadorServiceImplementation(JogadorRepository jogRepo) {
		this.jogRepo = jogRepo;

	}

	@Override
	public Jogador save(Jogador jogador) throws UnicidadeJogadorException, JogadorComNomeVazaioException {
		Jogador jogadorTemp = new Jogador();
		if (jogador.getNome().isEmpty() || jogador.getNome() == " ") {
			throw new JogadorComNomeVazaioException();
		}
		
		Optional<Jogador> optional = jogRepo.findByNome(jogador.getNome());
		if (optional.isPresent()) {
			throw new UnicidadeJogadorException();
		}
		jogadorTemp = jogRepo.saveAndFlush(jogador);

		
		return jogadorTemp;
	}

	@Override
	public List<Jogador> findAll() throws ListaDeJogadorVaziaException {
		System.out.println("findAll");
		List<Jogador> list = jogRepo.findAll();
		
		if (list.isEmpty()) {
			
			throw new ListaDeJogadorVaziaException();
		}
		
		
		return list;
	}

	@Override
	public Jogador update(Jogador jogador) throws JogadorComNomeVazaioException, UnicidadeJogadorException {
	
		if (jogador.getNome().isEmpty()) {
			throw new JogadorComNomeVazaioException();
		}
		
		Optional<Jogador> optional = jogRepo.findByNome(jogador.getNome());
		if (optional.isPresent()) {
			throw new UnicidadeJogadorException();
		}
		
		return jogRepo.save(jogador);
	}
	
	

}
