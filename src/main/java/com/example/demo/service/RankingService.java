package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Ranking;
import com.example.demo.service.except.JogadorComNomeVazaioException;
import com.example.demo.service.except.ListaDeRankingVaziaException;
import com.example.demo.service.except.RankingComValoresNegativosException;
import com.example.demo.service.except.UnicidadeDeJogadorPorRankingException;
import com.example.demo.service.except.UnicidadeJogadorException;


public interface RankingService {
	
	public Ranking save(Ranking rank) throws UnicidadeDeJogadorPorRankingException, RankingComValoresNegativosException, UnicidadeJogadorException, JogadorComNomeVazaioException;
	public Ranking update(Integer id, Ranking rank) throws UnicidadeDeJogadorPorRankingException, RankingComValoresNegativosException;
	public List<Ranking> findAllByIdOrderByVitoriasDesc() throws ListaDeRankingVaziaException;
}
