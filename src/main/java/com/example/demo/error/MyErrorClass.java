package com.example.demo.error;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MyErrorClass {

	public void sendError(HttpServletRequest request, HttpServletResponse response, String message, int httpStatusCode) throws IOException {
		System.out.println("error");
		response.setStatus(httpStatusCode);
		response.setContentType("application/json");

	    Map<String, Object> data = new HashMap<>();
	    data.put("timestamp", new Date());
	    data.put("status",402);
	    data.put("message", message);
	    data.put("statusText", message);
	    data.put("path", request.getRequestURL().toString());
	    
	    response.addHeader("Message", message );
	    
	    OutputStream out = response.getOutputStream();
	    ObjectMapper mapper = new ObjectMapper();
	    mapper.writeValue(out, data);
	    out.flush();
	}
	
}
